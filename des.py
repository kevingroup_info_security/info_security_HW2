__author__ = '懷民'

from des_func import *
from tkinter import *

root = Tk()
root.wm_title("DES")

def encode_com():
    if entryPt.get() != "" and entryKey.get() != "":
        pt = entryPt.get()
        k = entryKey.get()
        entryCt.delete(0, END)
        entryCt.insert(0, encode(pt, k))

def decode_com():
    if entryCt.get() != "" and entryKey.get() != "":
        ct = entryCt.get()
        k = entryKey.get()
        entryPt.delete(0, END)
        entryPt.insert(0, decode(ct, k))


lblPt = Label(root, text="Plaintext：").grid(row=0, sticky=E)
lblCt = Label(root, text="Ciphertext：").grid(row=1, sticky=E)
lblKey = Label(root, text="Key：").grid(row=2, sticky=E)
entryPt = Entry(root, width=70)
entryPt.focus_set()
entryCt = Entry(root, width=70)
entryKey = Entry(root, width=70)
btnEncode = Button(root, text="Encode", command=encode_com).grid(row=3, column=1)
btnDecode = Button(root, text="Decode", command=decode_com).grid(row=3, column=0)

entryPt.grid(row=0, column=1)
entryCt.grid(row=1, column=1)
entryKey.grid(row=2, column=1)

root.mainloop()