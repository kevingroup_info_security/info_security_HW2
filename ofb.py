__author__ = '懷民'

from des_func import *
from tkinter import *
from PIL import Image
import time

root = Tk()
root.wm_title("OFB")

def OFB():
    rgb = ""
    result = ""
    IV =  "1111101011111010111110101111101011111010111110101111101011111010"
    key = "1010111110101111101011111010111110101111101011111010111110101111"
    img = Image.open("cute-cougar-baby.bmp")
    pix = img.convert('RGB')
    for i in range(640):
        for j in range(480):
            r, g, b = pix.getpixel((i, j))
            rgb += '{0:08b}'.format(r)
            rgb += '{0:08b}'.format(g)
            rgb += '{0:08b}'.format(b)
    tic = time.clock()
    for i in range(int(len(rgb)/64)):
        IV = encode(IV, key)
        result += Xor(IV, rgb[i*64:(i+1)*64], 64)
    toc = time.clock()

    im = Image.new('RGB', (640, 480))
    pix2 = im.load()
    for i in range(im.size[0]):
        for j in range(im.size[1]):
            r = result[(i*480*24)+(j*24):(i*480*24)+(j*24+8)]
            g = result[(i*480*24)+(j*24+8):(i*480*24)+(j*24+16)]
            b = result[(i*480*24)+(j*24+16):(i*480*24)+((j+1)*24)]
            pix2[i, j] = (int(r, 2), int(g, 2), int(b, 2))

    im.save('ofb_encode.bmp')
    entrySec.delete(0, END)
    entrySec.insert(0, toc-tic)

def OFB_de():
    rgb = ""
    result = ""
    IV =  "1111101011111010111110101111101011111010111110101111101011111010"
    key = "1010111110101111101011111010111110101111101011111010111110101111"
    img = Image.open("ofb_encode.bmp")
    pix = img.convert('RGB')
    for i in range(640):
        for j in range(480):
            r, g, b = pix.getpixel((i, j))
            rgb += '{0:08b}'.format(r)
            rgb += '{0:08b}'.format(g)
            rgb += '{0:08b}'.format(b)
    tic = time.clock()
    for i in range(int(len(rgb)/64)):
        IV = encode(IV, key)
        result += Xor(IV, rgb[i*64:(i+1)*64], 64)
    toc = time.clock()

    im = Image.new('RGB', (640, 480))
    pix2 = im.load()
    for i in range(im.size[0]):
        for j in range(im.size[1]):
            r = result[(i*480*24)+(j*24):(i*480*24)+(j*24+8)]
            g = result[(i*480*24)+(j*24+8):(i*480*24)+(j*24+16)]
            b = result[(i*480*24)+(j*24+16):(i*480*24)+((j+1)*24)]
            pix2[i, j] = (int(r, 2), int(g, 2), int(b, 2))

    im.save('ofb_decode.bmp')
    entrySec.delete(0, END)
    entrySec.insert(0, toc-tic)


btnEncode = Button(root, text="Encode by OFB", command=OFB).grid(row=0, column=0)
btnDecode = Button(root, text="Decode by OFB", command=OFB_de).grid(row=1, column=0)
lblTime = Label(root, text="Processing Time：").grid(row=2, column=0)
entrySec = Entry(root)
entrySec.grid(row=3, column=0)

root.mainloop()
